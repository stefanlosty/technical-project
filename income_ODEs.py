# -*- coding: utf-8 -*-
"""
Created on Fri May 21 07:28:05 2021

@author: SurfacePro3
"""
from scipy.integrate import odeint
import numpy as np
import matplotlib.pyplot as plt

def sin_curve(x0,t,pars):
    x_0, amplitude, frequency = pars
    amplitude = amplitude*x_0
    dxdt = amplitude*frequency*np.cos(frequency*t)
    return dxdt

def exponential(x0,t,pars):
    coefficient, x_0 = pars
    dxdt = coefficient*x0
    return dxdt

def exp_curve(x0,t,pars):
    coefficient, x_0 = pars
    dxdt = x_0*np.log(coefficient)*coefficient**t
    return dxdt

def exp_sin(x0,t,pars):
    coefficient, x_0, frequency = pars
    dxdt = x_0*coefficient**t*(np.log(coefficient)*np.sin(frequency*t)+frequency*np.cos(frequency*t))
    return dxdt

def exp_decay(x0,t,pars):
    coefficient, x_0 = pars
    dxdt = -x_0*np.log(coefficient)/coefficient**t
    return dxdt

def indice_curve(x0,t,pars):
    coefficient, x_0 = pars
    dxdt = np.log(coefficient)*(np.log(x_0)*coefficient**t)*x_0**coefficient**t
    return dxdt

def indice_sin(x0,t,pars):
    coefficient, x_0, frequency = pars
    dxdt = np.log(coefficient)*(np.log(x_0)*coefficient**t)*x_0**coefficient**t
    dxdt = (x_0**coefficient**t)*((coefficient**t)*np.log(coefficient)*np.log(x_0)*np.sin(frequency*t)+frequency*np.cos(frequency*t))
    return dxdt

def constant_curve(x0,t,pars):
    coefficient, x_0 = pars
    dxdt = coefficient*x_0
    return dxdt

def polarise(x0,t,pars):
    coefficient, threshold, x_0 = pars
    if x0 > threshold:
        dxdt = x_0*coefficient*t**(coefficient-1)
    if x0 < threshold:
        dxdt = -x_0*coefficient*t**(coefficient-1)
    if x0 == threshold:
        dxdt = 0
    return dxdt
    
def solve(ode_f,x0,t,pars):
    time_series = odeint(ode_f,x0,t,(pars,))
    return time_series

def output_matrix(initial_distribution,initial_min,initial_max,t,ode_f,pars):
    income_minimums = np.zeros(len(t))
    income_maximums = np.ndarray.flatten(solve(ode_f,initial_max,t,pars))
    P = len(initial_distribution)
    timesteps = len(t)
    income_matrix = np.zeros((timesteps,P))
    x_0, amplitude, frequency  = pars
#    coefficient, threshold, x_0  = pars
#    coefficient, x_0 = pars
    for j in range(P):
        pars = initial_distribution[j], amplitude, frequency
#        pars = coefficient, threshold, initial_distribution[j]
#        pars = coefficient, initial_distribution[j]
        y = solve(ode_f,initial_distribution[j],t,pars)
        income_matrix[:,j] = np.ndarray.flatten(y)
    return income_matrix,income_minimums,income_maximums

#coeff = 1
#income_matrix2,income_minimums2,income_maximums2 = output_matrix(income_matrix[0],initial_min_income,initial_max_income,t,indice_curve,(1.002,0))