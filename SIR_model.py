# -*- coding: utf-8 -*-
"""
Created on Mon Mar 22 22:53:18 2021

@author: SurfacePro3
"""
from scipy.integrate import odeint
import numpy as np
from matplotlib.pyplot import *

def ODEs(U,t):
    global M
    global I
    global R
    global S_idt
    global dIdt
    global dRdt
    global derivatives
    M = len(U)-2
    susceptibles = U[:M]
    I = U[M]
    R = U[M+1]
    S_idt = []
    if I >= alpha:
        c = 0
    else:
        c = gamma
    for i in range(M):
        S_idt.append((-1/(i+1)*susceptibles[i]*I**(i+1))/N)
    dIdt = -(sum(S_idt))/N - c*I
    dRdt = c*I
    S_idt.append(dIdt)
    S_idt.append(dRdt)
    return S_idt
    
def solve(ode_f,U):
    time_series = odeint(ode_f,initial_conditions,t_span)
    return time_series

def plot_solution(t_span,susceptibles,str):
    fig = figure(figsize = (8,6))
    ax = fig.add_subplot(1,1,1)
    for i in range(M):
        j = i+1
#        ax.plot(t_span,susceptibles[:,i], label = 'S%x' %j)
        ax.plot(t_span,susceptibles[:,i], label = 'S%x' %j)
    ax.plot(t_span,solution[:,M], label = 'I')
    ax.plot(t_span,solution[:,M+1], label = 'R')
    xlabel("Time")
    ax.legend(fontsize = 'large')
    title(str)
    grid() 
#    savefig('SIR_C5_standalone_0.18.png', dpi=400)

alpha = 0.5
gamma = 0.7
initial_conditions = [1,0,1e-6,0]
t_span = np.linspace(0, 200, 1001)
N = sum(initial_conditions)
solution = solve(ODEs,initial_conditions)

plot_solution(t_span,solution, "SIR Simulation")
