# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 15:18:11 2020

@author: SurfacePro3
"""
import random as rand
from statistics import *
from math import *
import income_vs_time_curves as ivtc
import income_ODEs as i_O
import numpy as np
from matplotlib.pyplot import *
from jenks_natural_breaks import get_jenks_breaks
from sklearn.cluster import KMeans
import operator

# global variables
population_size = 40
initial_min_income = 0
initial_max_income = 100000
timesteps = 200
t = np.linspace(0,timesteps-1,timesteps)
window_size = 20

def main(ODE,ODE_pars):
    global income_matrix, population
#    income_matrix,income_minimums,income_maximums = create_income_matrix(population_size,initial_min_income,initial_max_income,ODE,ODE_pars)
    income_matrix,income_minimums,income_maximums = multimodal_income_matrix(population_size,initial_min_income,initial_max_income,ODE,ODE_pars)
    population,peasants,workers,professionals,bourgeoisie,boundaries_matrix = four_classes(income_matrix,income_minimums,income_maximums)
#    population,peasants,boundaries_matrix = one_class(income_matrix,income_minimums,income_maximums)
    
    global rel_dep_matrix, peasants_rel_dep_matrix, workers_rel_dep_matrix, professionals_rel_dep_matrix, bourgeoisie_rel_dep_matrix
    peasants_rel_dep_matrix = relative_deprivation(peasants,[boundaries_matrix[0],boundaries_matrix[1]])
    workers_rel_dep_matrix = relative_deprivation(workers,[boundaries_matrix[1],boundaries_matrix[2]])
    professionals_rel_dep_matrix = relative_deprivation(professionals,[boundaries_matrix[2],boundaries_matrix[3]])
    bourgeoisie_rel_dep_matrix = relative_deprivation(bourgeoisie,[boundaries_matrix[3],boundaries_matrix[4]])
    rel_dep_matrix = np.concatenate([peasants_rel_dep_matrix,workers_rel_dep_matrix,professionals_rel_dep_matrix,bourgeoisie_rel_dep_matrix])
#    rel_dep_matrix = np.concatenate([peasants_rel_dep_matrix,workers_rel_dep_matrix])
#    rel_dep_matrix = peasants_rel_dep_matrix
    rel_dep_matrix = np.array(rel_dep_matrix)
    
    global moving_average_matrix, lost_income_ratio_matrix
    moving_average_matrix = moving_average(income_matrix)
    lost_income_ratio_matrix = lost_income_ratio(income_matrix,moving_average_matrix)
    
    global mean_lost_income_ratio
    revolutionary_sentiment_matrix = revolutionary_sentiment(population)
    mean_rev_sentiment = get_mean(revolutionary_sentiment_matrix)
    mean_lost_income_ratio = get_mean(lost_income_ratio_matrix)
    global cluster_matrix
    cluster_matrix = clustering(rel_dep_matrix,lost_income_ratio_matrix)
    
    return rel_dep_matrix, mean_lost_income_ratio, mean_rev_sentiment

class peasant:
    def __init__(self,income,rel_dep,lost_income_ratio,rev_sentiment):
        self.income = income
        self.rel_dep = rel_dep
        self.lost_income_ratio = lost_income_ratio
        self.rev_sentiment = rev_sentiment
        
class worker:
    def __init__(self,income,rel_dep,lost_income_ratio,rev_sentiment):
        self.income = income
        self.rel_dep = rel_dep
        self.lost_income_ratio = lost_income_ratio
        self.rev_sentiment = rev_sentiment

class professional:
    def __init__(self,income,rel_dep,lost_income_ratio,rev_sentiment):
        self.income = income
        self.rel_dep = rel_dep
        self.lost_income_ratio = lost_income_ratio
        self.rev_sentiment = rev_sentiment
        
class bourgeois:
    def __init__(self,income,rel_dep,lost_income_ratio,rev_sentiment):
        self.income = income
        self.rel_dep = rel_dep
        self.lost_income_ratio = lost_income_ratio
        self.rev_sentiment = rev_sentiment
       
def create_income_matrix(population_size,min_income,max_income,ODE,pars):
    initial_distribution = []
    for i in range(population_size):
        initial_distribution.append(rand.randrange(min_income,max_income))
    initial_distribution.sort()
    income_matrix,income_minimums,income_maximums = i_O.output_matrix(initial_distribution,min_income,max_income,t,ODE,pars)     
    return income_matrix,income_minimums,income_maximums

def multimodal_income_matrix(population_size,min_income,max_income,ODE,pars):
    X1 = np.random.normal(20000, 1000, int(0.25*population_size))
    X2 = np.random.normal(40000, 1000, int(0.25*population_size))
    X3 = np.random.normal(60000, 1000, int(0.25*population_size))
    X4 = np.random.normal(80000, 1000, int(0.25*population_size))
    initial_distribution = np.concatenate([X1,X2,X3,X4])
#    initial_distribution = np.concatenate([X1])
#    initial_distribution = X1
    initial_distribution.tolist()
    initial_distribution.sort()
    income_matrix,income_minimums,income_maximums = i_O.output_matrix(initial_distribution,min_income,max_income,t,ODE,pars)
    return income_matrix,income_minimums,income_maximums

def one_class(income_matrix,income_minimums,income_maximums):
    peasants = []
    population = []
    for i in range(population_size):
        peasants.append(peasant(income_matrix[:,i],[],[],[]))
        population.append(peasants[i])
    
    global boundaries_0, boundaries_1    
    boundaries_0 = income_minimums
    boundaries_1 = income_maximums
    boundaries_matrix = np.array([boundaries_0,boundaries_1]) 
    return population,peasants,boundaries_matrix

def two_classes(income_matrix,income_minimums,income_maximums):
    breaks = get_jenks_breaks(income_matrix[0], 2)
    a = np.nonzero(income_matrix[0] == breaks[1])[0][0]+1
    
    peasants = []
    workers = []

    population = []
    for i in range(a):
        peasants.append(peasant(income_matrix[:,i],[],[],[]))
        population.append(peasants[i])
    for i in range(a,population_size):
        workers.append(worker(income_matrix[:,i],[],[],[]))
        population.append(workers[i-a])
    
    global boundaries_0, boundaries_1,boundaries_2    
    boundaries_0 = income_minimums
    boundaries_1 = [];
    for i in range(timesteps):
        boundaries_1.append(mean([income_matrix[i,a-1],income_matrix[i,a]]))
    boundaries_2 = income_maximums
    boundaries_matrix = np.array([boundaries_0,boundaries_1,boundaries_2])
    
    return population,peasants,workers,boundaries_matrix

def four_classes(income_matrix,income_minimums,income_maximums):
    breaks = get_jenks_breaks(income_matrix[0], 4)
    a = np.nonzero(income_matrix[0] == breaks[1])[0][0]+1
    b = np.nonzero(income_matrix[0] == breaks[2])[0][0]+1
    c = np.nonzero(income_matrix[0] == breaks[3])[0][0]+1

    peasants = []
    workers = []
    professionals = []
    bourgeoisie = []
    population = []
    for i in range(a):
        peasants.append(peasant(income_matrix[:,i],[],[],[]))
        population.append(peasants[i])
    for i in range(a,b):
        workers.append(worker(income_matrix[:,i],[],[],[]))
        population.append(workers[i-a])
    for i in range(b,c):
        professionals.append(professional(income_matrix[:,i],[],[],[]))
        population.append(professionals[i-b])
    for i in range(c,population_size):
        bourgeoisie.append(bourgeois(income_matrix[:,i],[],[],[]))
        population.append(bourgeoisie[i-c])
    
    global boundaries_0, boundaries_1,boundaries_2, boundaries_3, boundaries_4 
    boundaries_0 = income_minimums
    boundaries_1 = []; boundaries_2 = []; boundaries_3 = []
    for i in range(timesteps):
        boundaries_1.append(mean([income_matrix[i,a-1],income_matrix[i,a]]))
        boundaries_2.append(mean([income_matrix[i,b-1],income_matrix[i,b]]))
        boundaries_3.append(mean([income_matrix[i,c-1],income_matrix[i,c]]))
    boundaries_4 = income_maximums
    boundaries_matrix = np.array([boundaries_0,boundaries_1,boundaries_2,boundaries_3,boundaries_4])
    
    return population,peasants,workers,professionals,bourgeoisie,boundaries_matrix

def assign_classes(income_matrix,income_minimums,income_maximums):
    a = int(population_size/4)
    b = int(population_size*2/4)
    c = int(population_size*3/4)
    
    peasants = []
    workers = []
    professionals = []
    bourgeoisie = []
    population = []
    for i in range(a):
        peasants.append(peasant(income_matrix[:,i],[],[],[]))
        population.append(peasants[i])
    for i in range(a,b):
        workers.append(worker(income_matrix[:,i],[],[],[]))
        population.append(workers[i-a])
    for i in range(b,c):
        professionals.append(professional(income_matrix[:,i],[],[],[]))
        population.append(professionals[i-b])
    for i in range(c,population_size):
        bourgeoisie.append(bourgeois(income_matrix[:,i],[],[],[]))
        population.append(bourgeoisie[i-c])
        
    boundaries_0 = income_minimums
    boundaries_1 = []; boundaries_2 = []; boundaries_3 = []
    for i in range(timesteps):
        boundaries_1.append(mean([income_matrix[i,a-1],income_matrix[i,a]]))
        boundaries_2.append(mean([income_matrix[i,b-1],income_matrix[i,b]]))
        boundaries_3.append(mean([income_matrix[i,c-1],income_matrix[i,c]]))
    boundaries_4 = income_maximums
    boundaries_matrix = np.array([boundaries_0,boundaries_1,boundaries_2,boundaries_3,boundaries_4])
    
    return population,peasants,workers,professionals,bourgeoisie,boundaries_matrix

def relative_deprivation(class_list,boundaries_pair):
    class_income_matrix = []
    for person in class_list:
        class_income_matrix.append(person.income)
    class_income_matrix = np.array(class_income_matrix)
    
    class_widths = []
    class_income_maximums = []
    for i in range(timesteps):
        class_widths.append(boundaries_pair[1][i]-boundaries_pair[0][i])
        class_income_maximums.append(max(class_income_matrix[:,i]))
    
    for i in range(timesteps):    
        for person in class_list:
            d = class_income_maximums[i]-person.income[i]
            D = boundaries_pair[1][i]-person.income[i]
            person.rel_dep.append(sqrt(d*D)/class_widths[i])
            
    class_rel_dep_matrix = []
    for person in class_list:
        class_rel_dep_matrix.append(person.rel_dep)    
    return class_rel_dep_matrix

def moving_average(income_matrix):
    moving_average_matrix = []
    for person in range(population_size):
        i = 0
        moving_average = []
        while i <= len(income_matrix[:,person])-window_size:
            window = income_matrix[i:i+window_size,person]
            window_average = sum(window)/window_size
            moving_average.append(window_average)
            i+=1
        moving_average_matrix.append(moving_average)
    moving_average_matrix = np.transpose(moving_average_matrix)
    return moving_average_matrix

def lost_income_ratio(income_matrix,moving_average_matrix):
    lost_income_ratio_matrix = []
    for person in range(population_size):
        instant_lost_income = []
        for i in range(len(moving_average_matrix[:,person])):
            instant_lost_income.append((moving_average_matrix[i,person]-income_matrix[i+window_size-1,person])/moving_average_matrix[i,person])
            
        cumulative_lost_income = np.zeros(len(moving_average_matrix[:,person]))
        lost_income_ratio = []
        for i in range(len(moving_average_matrix[:,person])):
            trigger = instant_lost_income[i-1]*instant_lost_income[i]
            if trigger > 0:
                cumulative_lost_income[i] = instant_lost_income[i]+cumulative_lost_income[i-1]
            elif trigger <= 0:
                cumulative_lost_income[i] = instant_lost_income[i]
            lost_income_ratio.append(cumulative_lost_income[i])
        lost_income_ratio_matrix.append(lost_income_ratio)
        population[person].lost_income_ratio = lost_income_ratio
    lost_income_ratio_matrix = np.transpose(lost_income_ratio_matrix)
    return lost_income_ratio_matrix

def revolutionary_sentiment(population):
    revolutionary_sentiment_matrix = np.empty([timesteps-window_size+1,population_size])
    for p in range(population_size):
        for i in range(timesteps-window_size+1):
            population[p].rev_sentiment.append(mean([population[p].rel_dep[i+window_size-1],population[p].lost_income_ratio[i]]))
            revolutionary_sentiment_matrix[i,p] = population[p].rev_sentiment[i]
    return revolutionary_sentiment_matrix

def get_mean(matrix):
    mean_array = []
    for i in range(timesteps-window_size+1):
        mean_array.append(mean(matrix[i]))
    return mean_array

def clustering(rel_dep_matrix,lost_income_ratio_matrix):
    index, value = max(enumerate(lost_income_ratio_matrix[:,0]), key=operator.itemgetter(1))
    print(index)
    cluster_matrix = np.array([lost_income_ratio_matrix[index],rel_dep_matrix[:,index]])
    cluster_matrix = np.transpose(cluster_matrix)
    n_clusters = 2
    
    k_means = KMeans(init='k-means++', n_clusters=2, n_init=10)
    k_means.fit(cluster_matrix)
    k_means_labels = k_means.labels_
    k_means_cluster_centers = k_means.cluster_centers_
    k_means_labels_unique = np.unique(k_means_labels)
    
#    k_means_labels = []
#    for i in range(23):
#        k_means_labels.append(1)
#    for i in range(17):
#        k_means_labels.append(0)
    k_means_labels = np.array(k_means_labels)
    #colors = ['#4EACC5', '#FF9C34', '#4E9A06', '#F5370E']
    colors = ['#4EACC5', '#FF9C34']
    figure()
    
    global my_members,my_legend,k,col
    for k, col in zip(range(n_clusters), colors):
        my_members = k_means_labels == k
        my_legend = ["Individuals in S1","Individuals in S4"]
        cluster_center = k_means_cluster_centers[k]
        plot(cluster_matrix[my_members, 0], cluster_matrix[my_members, 1], 'w',
                markerfacecolor=col, marker='o', label = my_legend[k])
#        plot(cluster_center[0], cluster_center[1], 'o', markerfacecolor=col,
#                markeredgecolor='k', markersize=6)
        print(sum(my_members))
    title('Ratio of Lost Income Against Relative Deprivation', fontsize = 14, weight = 'bold')
    xlabel('Ratio of Lost Income', fontsize=14)
    ylabel('Relative Deprivation', fontsize=14)
    legend()
    grid(True)
#    savefig("clustering_C1.png", dpi=400)
    show()
    return cluster_matrix

"""
Plot code is currently not well generalised.
"""
def save_plot(y,t,x_label,y_label,my_legend,title,filename):
    if my_legend == ["Selected individuals"]:
        for i in range(0,population_size,5):
            plot(t,y[i],'g')
        legend(my_legend)
    elif my_legend == ["Income","Moving Average"]:
        for i in range(len(y)):
            for j in range(2):
                plot(t[i],y[i][:,j],label = my_legend[i]+" %x"%(j+1))
                legend()
    elif my_legend == ["Constant","Exponential","Sinusoidal"]:
        for i in range(1):
#            plot(t,y[:,i],label = "Individual %x"%(i+1))
            plot(t,y[:,i])
#            legend()
    elif my_legend == "Individual":
        x = []
        for i in range(len(y)):
            x.append(i)
        bar(x,y, label = my_legend)
        legend()
    figtext(0.2,0.9,title, fontsize = 14, weight = 'bold')
    xlabel(x_label, fontsize=14)
    ylabel(y_label, fontsize=14)
#    savefig(filename,bbox_inches='tight', dpi=400)
    
#constant_d_m,constant_m_l_i_r,constant_m_r_s = main(ivtc.constant,-0.005)     
#exp_d_m,exp_m_l_i_r,exp_m_r_s = main(i_O.polarise,(0.1,65000,initial_max_income))
#exp_d_m,exp_m_l_i_r,exp_m_r_s = main(i_O.sin_curve,(initial_max_income,0.5,0.02*pi))
exp_d_m,exp_m_l_i_r,exp_m_r_s = main(i_O.constant_curve,(-0.0025,initial_max_income))
#exp_d_m,exp_m_l_i_r,exp_m_r_s = main(i_O.exp_curve,(1.002,initial_max_income))



t_shift = np.linspace(19,199,181)
#scatter(lost_income_ratio_matrix[0],rel_dep_matrix[:,0])
#save_plot(lost_income_ratio_matrix[:,[39]],t_shift,"Time","Ratio of Lost Income",["Constant","Exponential","Sinusoidal"],"Ratio of Lost Income Against Time","RLI_C5.png")
#save_plot([income_matrix[:,4],moving_average_matrix[:,4]],[t,t_shift],"Time","Value",["Income","Moving Average"],"Actual Income and Expected Income","ma.png")
#save_plot(exp_d_m,t,"Time","Relative Deprivation",["Selected individuals"],"Relative Deprivation Against Time","RD_C5.png")    
#save_plot([income_matrix[:,[0,37]],moving_average_matrix[:,[0,37]]],[t,t_shift],"Time","Value",["Income","Moving Average"],"Actual Income and Expected Income","ma_C5.png")
#save_plot(income_matrix[199],[t,t_shift],"Population","Income","Individual","Income Distribution at t=t_peak","income_C5_peak.png")
#save_plot(income_matrix[0],[t,t_shift],"Population","Income","Individual","Initial Income Distribution","income_C3.png")
#save_plot(rel_dep_matrix[:,0],[t,t_shift],"Population","Relative Deprivation","Individual","Relative Deprivation Across Population","bar_C3.png")
#cluster_matrix = clustering(rel_dep_matrix,lost_income_ratio_matrix)